const path = require("path");
const axios = require('axios');
const cors = require('@fastify/cors');


// Require the fastify framework and instantiate it
const fastify = require("fastify")({
  // set this to true for detailed logging:
  logger: false,
});

fastify.register(cors, {
    origin: '*',
  }
)

fastify.post("/", async function (request, reply) {
  try {
    const commit_message = 'upload-solution';
    const author_name = 'coding-challenges-app';

    const { challengeNumber, filename, content } = request.body;
    const branch = `challenge-${challengeNumber}`;
    const filePath = `src/solutions/${branch}/${filename}`;

    const data = { branch, content, commit_message, author_name };
    const url = `https://gitlab.com/api/v4/projects/${process.env.PROJECT_ID}/repository/files/${encodeURIComponent(filePath)}`;

    const result = await axios.post(url, data, { headers: { 'PRIVATE-TOKEN': process.env.ACCESS_TOKEN }});
  return reply.status(201).send();
  } catch (e) {
    console.error(e);
    throw e;
  }
});

// Run the server and report out to the logs
fastify.listen(
  { port: process.env.PORT, host: "0.0.0.0" },
  function (err, address) {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    console.log(`Your app is listening on ${address}`);
  }
);
